//! # Abstract grammar
//!
//! ```
//! // start: expr;
//! //
//! // stmt: 'return' expr;
//! //
//! // expr: mul_div_expr (('+'|'-') mul_div_expr)*;
//! //
//! // mul_div_expr: atom (('*'|'/') atom)*;
//! //
//! // atom: literal
//! //     | '(' expr ')'
//! //     ;
//! ```

pub mod lexer;
pub mod llvmgen;
pub mod parser;
pub mod vm;
pub mod vmgen;

use self::lexer::Lexer;
use self::lexer::Token;
use self::llvmgen::LLVMGenerator;
use self::parser::Parser;
use self::vmgen::VMGen;

#[derive(Debug)]
pub enum Expr {
    Literal(i32),
    Op(Op, Box<Expr>, Box<Expr>),
}

#[derive(Debug)]
pub enum Stmt {
    Return(Expr),
}

#[derive(Debug)]
pub enum Op {
    Add,
    Sub,
    Mul,
    Div,
}

fn main() {
    let mut s = String::new();

    std::io::stdin()
        .read_line(&mut s)
        .expect("Unable to read line");

    let mut l = Lexer::new(&s[..]);
    let mut p = Parser::new(&mut l);
    match p.parse_expr() {
        Ok(res) => {
            let mut gen = LLVMGenerator::new();

            let code = gen.finalize(&res);
            println!("{}", code);

            let mut vmgen = VMGen::new();
            vmgen.main_expr(&res);
            for (i, insn) in vmgen.get_insns().iter().enumerate() {
                eprintln!("{} {:?}", i, insn);
            }
            eprintln!("Result: {}", vm::run(vmgen.get_insns()));
        }
        Err(e) => eprintln!("Error: {}", e),
    }

    match l.next_token() {
        Ok(Token::Eof) => (),
        Ok(e) => eprintln!("Unexpected {:?}, expected Eof", e),
        _ => (),
    }
}
