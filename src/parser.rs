use self::lexer::{Lexer, Token};
use super::lexer;
use super::{Expr, Op};

pub struct Parser<'a, 'b: 'a> {
    input: &'a mut Lexer<'b>,
}

impl<'a, 'b> Parser<'a, 'b> {
    pub fn new<'c, 'd: 'c>(input: &'c mut Lexer<'d>) -> Parser<'c, 'd> {
        Parser { input }
    }

    fn parse_prim(&mut self) -> Result<Expr, String> {
        match self.input.next_token()? {
            Token::LParen => {
                let exp = self.parse_expr()?;
                match self.input.next_token()? {
                    Token::RParen => Ok(exp),
                    x => Err(format!("Unexpected {:?}, expecting RParen", x)),
                }
            }
            Token::Number(a) => Ok(Expr::Literal(a)),
            o => Err(format!("Unexpected {:?}, expecting LParen or Number", o)),
        }
    }

    fn parse_mul_div(&mut self) -> Result<Expr, String> {
        let mut lhs = self.parse_prim()?;

        loop {
            match self.input.peek()? {
                Token::Mul => {
                    self.input.next_token()?;
                    lhs = Expr::Op(Op::Mul, Box::new(lhs), Box::new(self.parse_prim()?));
                }
                Token::Div => {
                    self.input.next_token()?;
                    lhs = Expr::Op(Op::Div, Box::new(lhs), Box::new(self.parse_prim()?));
                }
                _ => break,
            }
        }

        Ok(lhs)
    }

    fn parse_add_sub(&mut self) -> Result<Expr, String> {
        let mut lhs = self.parse_mul_div()?;

        loop {
            match self.input.peek()? {
                Token::Plus => {
                    self.input.next_token()?;
                    lhs = Expr::Op(Op::Add, Box::new(lhs), Box::new(self.parse_mul_div()?));
                }
                Token::Minus => {
                    self.input.next_token()?;
                    lhs = Expr::Op(Op::Sub, Box::new(lhs), Box::new(self.parse_mul_div()?));
                }
                _ => break,
            }
        }

        Ok(lhs)
    }

    pub fn parse_expr(&mut self) -> Result<Expr, String> {
        self.parse_add_sub()
    }
}
