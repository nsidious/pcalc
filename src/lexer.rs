use std::iter::Peekable;
use std::str::Chars;

#[derive(Clone)]
pub struct Lexer<'a> {
    input: Peekable<Chars<'a>>,
    tok_shift: Vec<Token>,
}

#[derive(Clone)]
pub struct LexerSaveState<'a> {
    tok_shift: Vec<Token>,
    input_iter: Peekable<Chars<'a>>,
}

#[derive(Debug, Clone)]
pub enum Token {
    Number(i32),
    LParen,
    RParen,
    Plus,
    Minus,
    Mul,
    Div,
    Eof,
}

impl<'a> Lexer<'a> {
    pub fn new(input: &str) -> Lexer {
        Lexer {
            input: input.chars().peekable(),
            tok_shift: Vec::new(),
        }
    }

    pub fn save_state(&self) -> LexerSaveState<'a> {
        LexerSaveState {
            input_iter: self.input.clone(),
            tok_shift: self.tok_shift.clone(),
        }
    }

    pub fn restore_state(&mut self, state: LexerSaveState<'a>) {
        self.input = state.input_iter;
        self.tok_shift = state.tok_shift;
    }

    fn peek_input(&mut self) -> Option<char> {
        self.input.peek().cloned()
    }

    pub fn unshift(&mut self, t: Token) {
        self.tok_shift.push(t);
    }

    fn next_input(&mut self) -> Option<char> {
        self.input.next()
    }

    fn skip_ws(&mut self) {
        while let Some(c) = self.peek_input() {
            if c == ' ' || c == '\t' || c == '\n' {
                self.next_input();
            } else {
                break;
            }
        }
    }

    pub fn peek(&mut self) -> Result<&Token, String> {
        let res = self.next_token()?;
        self.unshift(res);
        Ok(&self.tok_shift[self.tok_shift.len() - 1])
    }

    pub fn next_token(&mut self) -> Result<Token, String> {
        if let Some(t) = self.tok_shift.pop() {
            return Ok(t);
        }
        self.skip_ws();
        if let Some(c) = self.peek_input() {
            if c.is_digit(10) {
                let mut s = String::new();

                s.push(self.next_input().unwrap());
                while self.peek_input().map(|c| c.is_digit(10)) == Some(true) {
                    s.push(self.next_input().unwrap());
                }

                return Ok(Token::Number(s.parse().unwrap()));
            } else if c == '(' {
                self.next_input();
                return Ok(Token::LParen);
            } else if c == ')' {
                self.next_input();
                return Ok(Token::RParen);
            } else if c == '+' {
                self.next_input();
                return Ok(Token::Plus);
            } else if c == '-' {
                self.next_input();
                return Ok(Token::Minus);
            } else if c == '*' {
                self.next_input();
                return Ok(Token::Mul);
            } else if c == '/' {
                self.next_input();
                return Ok(Token::Div);
            } else {
                return Err(format!("Unrecognized character '{}'", c));
            }
        } else {
            return Ok(Token::Eof);
        }
    }
}
