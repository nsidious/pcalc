#include <stdint.h>
#include <stddef.h>
#include <stdio.h>

extern int32_t run(void);

int main() {
    printf("%d\n", run());
}
